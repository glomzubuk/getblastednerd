﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameplayEntities;
using LLHandlers;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using LLBML.Players;
using LLBML.States;
using LLBML.Math;
using LLBML;
using LLBML.Networking;

namespace GetBlastedNerd
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    public class TheMetaCrippler : BaseUnityPlugin
    {
        public static ManualLogSource Log { get; private set; }
        internal static TheMetaCrippler Instance { get; private set; }

        internal static ConfigEntry<KeyCode> getHitKey;
        internal static ConfigEntry<int> defaultDamageAmount;
        internal static ConfigEntry<int> knockDirection;
        internal static ConfigEntry<bool> doEffects;
        internal static ConfigEntry<bool> doDamage;

        private void Awake()
        {
            Instance = this;
            Log = this.Logger;
            Logger.LogInfo("Hello, world!");

            getHitKey = this.Config.Bind<KeyCode>("Keybinds", "GetHitKey", KeyCode.B);


            doEffects = this.Config.Bind<bool>("Toggles", "DoEffects", true);
            doDamage = this.Config.Bind<bool>("Toggles", "DoDamage", true);

            defaultDamageAmount = this.Config.Bind<int>("Values", "DefaultDamageAmount", 32, new ConfigDescription(
                "The amount of damage done when pressing the key",
                new AcceptableValueRange<int>(0,240)
            ));

            knockDirection = this.Config.Bind<int>("Values", "knockDirection", 1, new ConfigDescription(
                "Knock direction options: 1 = Back, 2 = Left, 3 = Right, 4 = Random",
                new AcceptableValueRange<int>(1, 4)
            ));

        }

        void Start()
        {
            LLBML.Utils.ModDependenciesUtils.RegisterToModMenu(this.Info, new List<string> {
                "Knock direction options: 1 = Back, 2 = Left, 3 = Right, 4 = Random"
            });
        }

        void Update()
        {
            if (!NetworkApi.IsOnline && GameStates.IsInMatch())
            {

                if (Input.GetKeyDown(getHitKey.Value))
                {
                    PlayerEntity pe = Player.GetLocalPlayer().playerEntity;
                    GetHitByNerd(pe);
                }
            }
        }


        private void GetHitByNerd(PlayerEntity playerEntity)
        {
            playerEntity.InterruptAbilities();
            playerEntity.PlayAnim("getHit", "main");
            playerEntity.attackingData.gravityOff = false;
            playerEntity.ReleaseHitEntities();
            Floatf damage = 0;
            Vector2f pushDirection = GetDirection(playerEntity.playerData.flyDirection);
            if (!DebugSettings.instance.dontLoseHP && doDamage.Value)
            {
                damage = GetDamage();
                playerEntity.attackingData.hp -= damage;
                if (DebugSettings.instance.instaKill)
                {
                    playerEntity.attackingData.hp -= (Floatf)9999999;
                }
            }
            if (playerEntity.attackingData.hp <= (Floatf)0)
            {
                GameCamera.instance.StartShake((Floatf)0.5m, (Floatf)0.4m, pushDirection);
                if (doEffects.Value)
                    playerEntity.effectHandler.CreateKillPlayerEffect(playerEntity.GetPosition(), pushDirection, (Floatf)64);
                playerEntity.GetKilled((Floatf)0.5m, pushDirection);
            }
            else
            {
                StartHitstun(playerEntity);
                GameCamera.instance.StartShake((Floatf)0.2m, (Floatf)0.4m, pushDirection);
                if (playerEntity.GetPixelFlySpeed(true) > (Floatf)100)
                {
                    GameCamera.StartWave(playerEntity.GetPosition(), false, default(HHBCPNCDNDH));
                }
                if (doEffects.Value)
                    playerEntity.effectHandler.CreateGetHitEffect(playerEntity.GetPosition(), pushDirection, (Floatf)64);
                playerEntity.PlayGetHitSfx();
            }
        }
        private Floatf GetDirection()
        {
            return (defaultDamageAmount.Value / World.INSTAKILL_VELOCITY) + 0.0001m;
        }

        private Floatf GetDamage()
        {
            return ((Floatf)defaultDamageAmount.Value / (Floatf)World.INSTAKILL_VELOCITY) + 0.0001m;
        }

        private void StartHitstun(PlayerEntity playerEntity)
        {
            if (GetDamage() > 0.251m || defaultDamageAmount.Value > 40 || DebugSettings.instance.alwaysFullKnockback)
            {
                playerEntity.StartHitstun(playerEntity.bigKnockbackDuration, HitstunState.HIT_BY_BALL_BIG_KNOCKBACK_STUN);
            }
            else if (defaultDamageAmount.Value > 18)
            {
                playerEntity.StartHitstun(playerEntity.mediumKnockbackDuration, HitstunState.HIT_BY_BALL_MEDIUM_KNOCKBACK_STUN);
            }
            else
            {
                playerEntity.StartHitstun(playerEntity.smallKnockbackDuration, HitstunState.HIT_BY_BALL_SMALL_KNOCKBACK_STUN);
            }
        }

        private Vector2f GetDirection(Vector2f playerFlyDirection)
        {
            switch (knockDirection.Value)
            {
                case 2:
                    return Vector2f.left;
                case 3:
                    return Vector2f.right;
                case 4:
                    switch (UnityEngine.Random.Range(0, 2))
                    {
                        case 0: return playerFlyDirection;
                        default: return -1 * playerFlyDirection;
                    }
                default:
                    return -1 * playerFlyDirection;
            }
        }
    }
}
